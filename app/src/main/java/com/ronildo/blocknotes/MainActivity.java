package com.ronildo.blocknotes;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    static ArrayList<String> notes = new ArrayList<>();
    static ArrayList<Integer> notesIds = new ArrayList<>();
    static ArrayAdapter arrayAdapter;
    static SQLiteDatabase database;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_note_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if (item.getItemId() == R.id.add_note) {

            Intent intent = new Intent(getApplicationContext(), NoteEditorActivity.class);

            startActivity(intent);

            return true;

        }

        return false;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        try {
            database = this.openOrCreateDatabase("notes_data_base", MODE_PRIVATE, null);

            database.execSQL("CREATE TABLE IF NOT EXISTS notes (id INTEGER PRIMARY KEY,description VARCHAR)");

            Cursor cursor = database.rawQuery("SELECT * FROM notes", null);

            int descriptionIndex = cursor.getColumnIndex("description");
            int idIndex = cursor.getColumnIndex("id");

            cursor.moveToFirst();

            while (cursor != null) {
                notes.add(cursor.getString(descriptionIndex));
                notesIds.add(idIndex);
                cursor.moveToNext();
            }

        } catch (Exception e) { }

        ListView listView = findViewById(R.id.listView);

        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, notes);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(getApplicationContext(), NoteEditorActivity.class);
                intent.putExtra("noteId", position);
                startActivity(intent);
            }
        });


        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {

                final int itemToDelete = position;

                new AlertDialog.Builder(MainActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Tem certeza?")
                        .setMessage("Deseja deletar esta nota?")
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        notes.remove(itemToDelete);

                                        Log.i("ID_TO_REMOVE: ", String.valueOf(notesIds.get(itemToDelete)));
                                        database.execSQL("DELETE FROM notes WHERE id = " + notesIds.get(itemToDelete) );
                                        arrayAdapter.notifyDataSetChanged();
                                    }
                                }
                        )
                        .setNegativeButton("Não", null)
                        .show();

                return true;
            }

        });
    }
}
